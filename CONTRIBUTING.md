# Contributing
Please note we have a [code of conduct](#Code-of-Conduct), please follow it in all your
interactions with the project.

When contributing to this repository, please first:

- Discuss the change you wish to make via a GitLab issue.
- Add appropriate label(s) to the new issue.
- Any merge request which hasn't been discussed in an issue before won't be taken into consideration.

## Documentation 
The API documentation requires to run the sphinx builder from the python embeded in Blender.  
[Graphviz](https://graphviz.org/download/) must be installed.

### Unix
1. `cd docs/`
1. Install requirements using **Blender python executable**: `$blender_python_path -m pip install -r requirements.txt`
1. Set an environment variable to target **Blender binary executable** `export STAX_BL_PATH=/path/to/blender/binary`
1. Build the docs `make html`
1. Open the built `build/html/index.html` in a browser

### Windows
**You must consider installing a portable version (`.zip`) and use it instead of the system installed one (`.msi`). To avoid encountering writing permission errors.**

1. `cd docs/`
1. Install requirements using **Blender python executable**: `$blender_python_path -m pip install -r requirements.txt`
1. Set an environment variable to target **Blender binary executable** `set STAX_BL_PATH=/path/to/blender/binary.exe`
1. Build the docs  `make.bat html`
1. Open the built `build/html/index.html` in a browser

## How to
- If you want to code it, clone the repo in the blender scripts directory :
    - Windows = `C:\Users\${USER}\AppData\Roaming\Blender Foundation\Blender\${BLENDER_VERSION}\scripts\startup\bl_app_templates_user\`
    - Linux = `${HOME}/.config/blender/${BLENDER_VERSION}/scripts/startup/bl_app_templates_user/`

- When it's installed, you can access it via the splash screen, you'll see a *Stax* template.

## Release process
We use the [GitLab Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html#production-branch-with-gitlab-flow).

The `master` branch is the main and default branch. Branchs are continuously checked out from `master` for developing features, bug fixes, refactors... and then merged + squashed into `master`.

Releases are tagged on `master`.

### Development phases
Before a release, we will switch to an alpha and then a beta phase. Merging new features is stopped and the devs focus on bug fixing. New features can still be developed, but will wait until after the release tag to get merged into `master`.

#### Alpha
Stax is being tested by technical users (e.g. TDs, Devs...) to be sure nothing is broken and Stax is still usable in any case...

#### Beta
Stax is being tested by everyday users (e.g. Artists, Sups, Reviewers...) to be sure everything works and the new features are as perfect as they could be.

### Roadmap
You can check the expected features for the upcoming versions in the [milestones](https://gitlab.com/superprod/stax/-/milestones).

## Merge Request Process

1. Fork the repository.
1. Checkout a new branch from `master` with an explicit name regarding its content. **Keep one branch by feature, else you'll be asked to split.**
1. Code, [test](#Testing), make sure everything works and nothing is broken. You can make as much commits as you need, they'll be squashed in the end. Make sure you strictly follow the [coding style](#Coding-style).
1. Update the documentation if needed.
1. Write your tests if the features allows it (feel free to ask for assistance).
1. Rebase your branch on `master` before considering your job finished.
1. Create a merge request to `master`. **Your git history has to be clean.**
1. Apply the corrections from the reviews until it's considered finished. It may happen the `master` branch evolves during the review process and creates conflicts with your branch, you'll be asked to rebase your branch.
1. Be happy, your contribution has been merged! (thank you)
1. Wait until the next release is tagged to see everyone use your wonderful work.


## Testing
Stax development is highly demanding about stability. That's why testing your contribution by hand and by running anti-regression tests is inescapable. Be aware you might be asked to write related automatic tests when proposing a feature, otherwise it might not be approved to be merged in core.  
"But it does take time!", sure, and saves much more from everyone: A bug is a lot of time consumption at a big scale, for users and for core team members, while spending a little more time to write some tests from one person, will make life easier for everyone.

*NB: If you're bugfixing, writing related tests to make sure the bug won't happen again is considerably welcome, but not mandatory.*

### By hand
As Stax core focuses on remaining simple and having distinct processes as much as possible, randomly test your feature, try to run something totally different during it's process, play with it until you break it! If you despair to make it crash, your feature is ready to be submitted to the [review process](#Merge-Request-Process)...

### Automatic tests
Automatic tests (a.k.a anti-regression tests) must be applied to all processes importing, exporting and changing the Stax/Blender data. They cannot be used to test UI behaviour (then, do it by hand!).

#### Run tests
Stax is using Pytest via a [fork](https://github.com/Tilix4/blender-addon-tester/tree/app_template) of [Blender Addon Tester](https://github.com/nangtani/blender-addon-tester) to run tests into Blender.

*NB: It's recommended to use a [Virtual Env](https://docs.python.org/3/library/venv.html#creating-virtual-environments).*

From `stax` directory: 
1. cd `./python/tests/utils/`
1. `pip install -r requirements.txt`
1. Set an environment variable to target **Blender binary executable** 
   - Unix: `export STAX_BL_PATH=/path/to/blender/binary`
   - Win: `set STAX_BL_PATH=/path/to/blender/binary.exe`
1. `python3 run_tests.py`

## Naming convention

### Branch
Clear and concise naming with a short prefix to explicit the issue number and then the kind of change:

- Refactor: `123_re_`
- Bugfix: `123_bf_`
- Feature: `123_ft_`
- Cleaning: `123_cl_`
- Documentation: `123_doc_`

### Commit
The commit message must start with the issue number, pipe, and the kind of changes which has been made, followed by a short description of the change. A longer description can be filled on the second line. Le last line must be the [gitlab closing pattern](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#default-closing-pattern).

> 123|Bugfix: Short description  
> Longer description  
> Closes #123 *(for a feature/refactor)*  
> Fix #123 *(for a bugfix)*  

## Coding style

### Comment your code!
Stax's code is highly commented and contributing to Stax implies to adopt this reflex. The global algorithm must be understandable by reading only the comments.

### Explicit variable name
We prefer a long and explicit variable name than a short and obscur one. 

- `timeline_item_path` is perfect, when `tip` or `tlitmpt` are unacceptable.
- A shortened name is still possible as long as it's perfectly obvious: `items_dir`.

### Docstrings
#### One line docstring
```python
"""Do this action"""
```

- Rules are:
  - Use """ and not '''
  - No space before the first letter
  - No space after the last letter
  - No point at the end of the line
  - Use imperative mode

##### Multi line docstring

```python
"""Do this action

To make the world a better place.
"""
```

- Rules are:
  - Use """ and not '''
  - No space before the first letter
  - Last """ need to be alone on his line
  - No point at the end of the first line
  - Use imperative mode in the first line
  - Only one sentence on first line.
  - Add space between first line and next ones.

#### Parameters and return

Use sphinx syntax with [annotations]( https://pypi.org/project/sphinx-autodoc-annotation/).

```python
# Standard
def f(a: AttrType) -> ReturnType:
    """Do something

    :param a: description for a
    :return: description for return
    """

# Example
def do_this(first_param: str, second_param: int) -> bool:
    """Do this

    :param first_param: description for first_param
    :param second_param: description for second_param
    :return: description for return
    """
```

### `import` statements order:

1. Standard Python (sys, os...)
1. Third parties (OTIO, ORIO, Blender)
1. Stax

#### Order
They have to be alphabetically ordered.

```python
# Wrong
import sys
import os
import unittest

# Right
import os
import sys
import unittest
```

### Linting
[Black](https://github.com/psf/black) is used with [flake8](https://flake8.pycqa.org/en/latest/).

## Code of Conduct

### Our Pledge

In the interest of fostering an open and welcoming environment, we as
contributors and maintainers pledge to making participation in our project and
our community a harassment-free experience for everyone, regardless of age, body
size, disability, ethnicity, gender identity and expression, level of
experience, nationality, personal appearance, race, religion, or sexual identity
and orientation.

### Our Standards

Examples of behavior that contributes to creating a positive environment
include:

* Using welcoming and inclusive language
* Being respectful of differing viewpoints and experiences
* Gracefully accepting constructive criticism
* Focusing on what is best for the community
* Showing empathy towards other community members

Examples of unacceptable behavior by participants include:

* The use of sexualized language or imagery and unwelcome sexual attention or
  advances
* Trolling, insulting/derogatory comments, and personal or political attacks
* Public or private harassment
* Publishing others' private information, such as a physical or electronic
  address, without explicit permission
* Other conduct which could reasonably be considered inappropriate in a
  professional setting

### Our Responsibilities

Project maintainers are responsible for clarifying the standards of acceptable
behavior and are expected to take appropriate and fair corrective action in
response to any instances of unacceptable behavior.

Project maintainers have the right and responsibility to remove, edit, or
reject comments, commits, code, wiki edits, issues, and other contributions
that are not aligned to this Code of Conduct, or to ban temporarily or
permanently any contributor for other behaviors that they deem inappropriate,
threatening, offensive, or harmful.

### Scope

This Code of Conduct applies both within project spaces, communication utilities and in public spaces
when an individual is representing the project or its community. Examples of
representing a project or community include using an official project e-mail
address, posting via an official social media account, or acting as an appointed
representative at an online or offline event. Representation of a project may be
further defined and clarified by project maintainers.

### Enforcement

Instances of abusive, harassing, or otherwise unacceptable behavior may be
reported by contacting the project team. All complaints will be reviewed and
investigated and will result in a response that is deemed necessary and
appropriate to the circumstances. The project team is obligated to maintain
confidentiality with regard to the reporter of an incident. Further details of
specific enforcement policies may be posted separately.

Project maintainers who do not follow or enforce the Code of Conduct in good
faith may face temporary or permanent repercussions as determined by other
members of the project's leadership.

### Attribution

This Code of Conduct is adapted from the [Contributor Covenant][homepage],
version 1.4, available at [http://contributor-covenant.org/version/1/4][version]

[homepage]: http://contributor-covenant.org
[version]: http://contributor-covenant.org/version/1/4/


