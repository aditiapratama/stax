# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""
Every utils function related to cache management
"""

import os
import platform
import shutil
import subprocess
import tempfile
from datetime import datetime, timedelta
from pathlib import Path
from typing import List, Union

import bpy
import opentimelineio as otio

from . import utils_core


def get_default_cache_directory():
    """Get Stax default cache directory path

    :return: Stax default cache directory path as string
    """
    return (
        Path(os.getenv("TEMP", tempfile.gettempdir()))
        .joinpath("stax")
        .resolve()
        .as_posix()
    )


def get_cache_directory():
    """Get Stax cache directory path

    :return: Stax cache directory path as string
    """
    user_cache_dir = (
        Path(bpy.context.scene.user_preferences.cache_directory).resolve()
        if hasattr(bpy.context, "scene")
        else get_default_cache_directory()
    )

    return user_cache_dir


def get_all_sessions_cache_directory() -> Path:
    """Get Stax all sessions directory path

    :return: Stax sessions directory
    """
    return Path(get_cache_directory(), "Sessions").resolve()


def get_session_cache_directory(timeline_path: Union[str, Path]) -> Path:
    """Get Stax session related to a timeline directory path

    :param timeline_path: Source timeline path to load session
    :return: Stax sessions directory
    """
    timeline_path = Path(timeline_path)
    return Path(get_all_sessions_cache_directory(), timeline_path.stem)


def get_session_cache_file(timeline_path: Union[str, Path]) -> Path:
    """Get session .blend file path

    :param timeline_path: Timeline to load session
    :return: Stax session .blend file path
    """
    timeline_path = Path(timeline_path)
    return get_session_cache_directory(timeline_path).joinpath(
        f"{timeline_path.stem}.blend"
    )


def start_from_default_file(session_file: Union[Path, str]):
    """Get default startup Stax file and save it

    :param session_file: Path to session file.
    """
    # Start from default file
    bpy.ops.wm.read_homefile()

    # If directory doesn't exist, create it
    if not session_file.parent.is_dir():
        session_file.parent.mkdir(parents=True)

    # Save session file
    bpy.ops.wm.save_as_mainfile(filepath=session_file.as_posix())


def get_media_cache_directory() -> Path:
    """Get media cache directory

    It targets a "Media" directory in the session's folder.

    :return: Media cache directory
    """
    return (
        get_session_cache_directory(Path(bpy.context.scene.timeline))
        .resolve()
        .joinpath("Media")
    )


def get_orio_default_cache_directory() -> Path:
    """Get Stax default reviews directory path.

    :return: Stax reviews directory
    """
    return Path(get_cache_directory(), "OpenReview").resolve()


def get_temp_directory() -> Path:
    """Get Stax temp directory path.

    :return: Stax temp directory path
    """
    return Path(get_cache_directory(), "tmp")


def read_cache_timeline(timeline_path: Union[Path, str]):
    """Returns the current timeline cache data from .json

    :param timeline: Source timeline path
    :return: Cache timeline as OpenTimeline IO object
    """
    timeline_path = Path(timeline_path)  # Sentinel
    timeline_path = Path(
        get_timeline_cache_dir(timeline_path), f"{timeline_path.stem}.otio"
    )

    if timeline_path.is_file():
        return otio.adapters.read_from_file(str(timeline_path))
    else:
        return None


def write_cache_timeline(
    timeline_path: Union[Path, str], timeline: otio.schema.Timeline
):
    """Returns the current timeline cache data from .json

    :param timeline_name: Timeline name
    :param timeline: Timeline timeline OpenTimelineIO object
    """
    timeline_path = Path(timeline_path)
    timeline_path = Path(
        get_timeline_cache_dir(timeline_path), f"{timeline_path.stem}.otio"
    )

    otio.adapters.write_to_file(timeline, str(timeline_path))


def get_timeline_cache_dir(timeline_path: Union[Path, str]) -> Path:
    """Get the current timeline cache directory path

    Same as session.

    :param timeline_path: Timeline path
    :param create_directory: Boolean to create or not the asked directory
    :return: Timeline cache directory path
    """
    timeline_path = Path(timeline_path)
    return get_session_cache_directory(timeline_path)


def get_timeline_cache_file_path(timeline_path: Union[Path, str]) -> Path:
    """Get the given timeline cache file path

    :param timeline_path: Timeline path
    :return: .otio cache timeline file path
    """
    timeline_path = Path(timeline_path)

    tl_cache_dir = get_timeline_cache_dir(timeline_path)

    timeline_cache_file = tl_cache_dir.joinpath(timeline_path.name).with_suffix(".otio")

    return timeline_cache_file


def copy_media_to_cache(filepath: Union[Path, str]):
    """Copy the media to Stax' cache

    :param filepath: Media to copy in cache.
    :return: Cache path as Path
    """
    media_source = Path(filepath).resolve()

    # Test if source drive is connected
    # Useful for media paths given by production tracking tools
    if not Path(media_source.drive).exists():
        error_message = f"{filepath.drive} drive not mounted !"
        raise OSError(error_message)

    # Test if file exists, if not, warn the user and return empty string
    if not media_source.is_file():
        utils_core.report_message("WARNING", f"No media at: {media_source}")
        return ""

    media_cache = get_media_cache_directory().joinpath(media_source.name)

    # Check if file is already copied, then return path
    if media_cache.is_file():
        return media_cache

    # Create directory if doesn't exist
    clip_dir = media_cache.parent
    if not clip_dir.is_dir():
        clip_dir.mkdir(parents=True)

    # Copy source media to cache
    if platform.system() == "Windows":
        command_settings = ["/njh", "/njs", "/ndl", "/nc", "/ns"]
        copy_command = [
            "robocopy",
            str(media_source.parent),
            str(media_cache.parent),
            str(media_source.name),
        ] + command_settings
    else:
        copy_command = ["cp", "-p", str(media_source), str(media_cache)]
    subprocess.call(copy_command)
    return media_cache


def delete_session_cache(timeline_path: Union[Path, str], full_reset=False):
    """Delete the session's .blend and .otio files cache of the given timeline

    :param full_reset: Delete also the cached media files
    """
    scene = bpy.context.scene

    session_cache_dir = get_session_cache_directory(timeline_path)

    if not session_cache_dir.is_dir():  # sentinel if session dir doesn't exist
        return

    if full_reset:  # Delete the whole session cache directory, with cached media
        shutil.rmtree(session_cache_dir, ignore_errors=True)
        bpy.ops.wm.report_message(
            type="INFO",
            message=f"{session_cache_dir} session cache directory (and media files if exist) deleted",
        )
    else:  # Delete only .blend and .otio cache files
        # .blend(1) files
        session_file = get_session_cache_file(timeline_path)
        if session_file.is_file():
            session_file.unlink()

        backup_file = session_file.with_suffix(".blend1")
        if backup_file.is_file():
            backup_file.unlink()

        # .otio file
        timeline_file = get_timeline_cache_file_path(timeline_path)
        if timeline_file.is_file():
            timeline_file.unlink()

        bpy.ops.wm.report_message(
            type="INFO", message=f"{session_cache_dir} session cache deleted"
        )


def clean_outdated_cache(
    expiration_delay=30, ignore_timelines: List[Union[Path, str]] = ""
):
    """Clean cache when last open date is too old

    :param expiration_delay: Delay in days a cache is outdated
    :param ignore_timeline: Timeline (by path) to exclude
    """
    sessions_cache_directory = get_all_sessions_cache_directory()
    ignored_directories = [get_session_cache_directory(tl) for tl in ignore_timelines]

    # Check every .otio for last modification and delete folder eventually
    for session in sessions_cache_directory.iterdir():
        if session.is_dir() and session not in ignored_directories:
            for child in session.iterdir():
                if child.suffix == ".otio":
                    last_date_accepted = datetime.today() - timedelta(
                        days=expiration_delay
                    )
                    cache_date = datetime.fromtimestamp(child.stat().st_mtime)

                    # If the last modification date of the json is more than the threshold and this is not the entity we
                    # are going to open with LoadPreviousSession then remove the directory
                    if cache_date < last_date_accepted:
                        shutil.rmtree(session)
                        bpy.ops.wm.report_message(
                            type="INFO",
                            message=f"Removed outdated cache of timeline {ignore_timelines[0]}",
                        )
                        break


def save_session():
    """Save the current session and create a .restore copy for aborting"""
    # Save .blend file
    if bpy.data.filepath:
        bpy.ops.wm.save_mainfile()
    else:  # Default saving
        bpy.ops.wm.save_mainfile(
            filepath=get_temp_directory().joinpath("untitled.blend").as_posix()
        )

    # Copy the updated file as a backup for restoring
    shutil.copy(bpy.data.filepath, Path(bpy.data.filepath).with_suffix(".restore"))
