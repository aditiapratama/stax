# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""
Every utils function related to user config management
"""

import json
import os
import platform
from pathlib import Path

import bpy

from .utils_core import (
    decode_password,
    encode_password,
    report_message,
)


def read_user_config(logger=report_message) -> dict:
    """Read the user config as json file into the app data directory

    :return: User config as dict
    """
    config_path = Path(get_config_directory() + "/cfg.json").resolve()

    # Default config file
    if not config_path.is_file():
        config_data = {}
        logger({"DEBUG"}, "Custom User Configuration not found, default loaded.")
    else:
        with config_path.open("r") as config_stream:
            config_data = json.load(config_stream)

    if hasattr(bpy.context, "view_layer"):
        logger({"DEBUG"}, f"Custom User Configuration found at: {config_path}")

    # Get rid of empty values
    config_data = {k: v for k, v in config_data.items()}

    return config_data


def load_user_config(logger=report_message):
    """Load the user config into Stax configuration user preferences"""
    config_data = read_user_config()

    # Load into user preferences
    if hasattr(bpy.context, "scene"):  # TODO meant to disappear
        user_preferences = bpy.context.scene.user_preferences
        for k, v in config_data.items():
            if k == "user_password":
                setattr(user_preferences, k, decode_password(v))
            else:
                try:
                    setattr(user_preferences, k, v)
                except TypeError as err:
                    print(err)
                    print(f"{k}: {v} can't be set for {user_preferences}")


def write_user_config(additional_data=dict()):
    """Write the user config as json file into the app data directory

    :param additional_data: Extra custom data to be added in the config
    """
    config_path = Path(get_config_directory(), "cfg.json").resolve()
    config_data = read_user_config()
    user_preferences = bpy.context.scene.user_preferences

    # If property group add it to config data
    if user_preferences:
        for a in user_preferences.__annotations__:
            # Make sure cache directory is an absolute path to both internal and saved config
            if a == "cache_directory":
                from .utils_cache import get_default_cache_directory

                cache_dir_setting = getattr(user_preferences, a, None)
                if cache_dir_setting in ("", None):
                    cache_dir_setting = get_default_cache_directory()
                absolute_cache_dir = Path(bpy.path.abspath(cache_dir_setting)).resolve()
                setattr(user_preferences, a, absolute_cache_dir.as_posix())

            # To make sure the enum returns the related string and not the int, use getattr()
            if a == "user_password":
                config_data[a] = encode_password(getattr(user_preferences, a))
            else:
                config_data[a] = getattr(user_preferences, a, "")

    # Add additionnal data
    config_data.update(additional_data)

    # Create directory if doesn't exit
    if not config_path.parent.is_dir():
        config_path.parent.mkdir(parents=True)

    # Write the config data to the config json file
    with config_path.open("w") as config_stream:
        json.dump(config_data, config_stream, indent=4)


def get_config_directory():
    """Get Stax config directory path

    :return: Stax default cache directory path as string
    """

    return (
        str(Path(os.getenv("APPDATA")).joinpath("Stax"))
        if platform.system() == "Windows"
        else str(Path.home().joinpath("stax"))
    )
