# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

"""
Every operator related to the local cache
"""
from pathlib import Path
from shutil import rmtree

import bpy


class CACHE_OT_purge_cache(bpy.types.Operator):
    """Purge all contents from Stax cache"""

    bl_idname = "scene.purge_cache"
    bl_label = "Purge Stax Cache"

    def invoke(self, context, _event):
        # Ask to confirm
        return context.window_manager.invoke_confirm(self, _event)

    def execute(self, context):
        cache_path = Path(context.scene.user_preferences.cache_directory).resolve()

        if cache_path.is_dir():
            for child in cache_path.iterdir():
                if child.is_dir():
                    rmtree(child)
                else:
                    child.unlink()

        self.report(type={"INFO"}, message="Cache purged!")

        return {"FINISHED"}


classes = [CACHE_OT_purge_cache]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)


def unregister():
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
