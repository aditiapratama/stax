# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""
Every operator relative to version
"""
from pathlib import Path
from typing import List

import bpy
from bpy_extras.io_utils import ImportHelper
from bpy.types import MetaSequence, SoundSequence

from stax.utils import utils_cache, utils_linker, utils_timeline
from stax.utils.utils_core import (
    get_context,
    get_displayed_tracks,
)
from stax.utils.utils_note import clear_advanced_drawings, clear_annotate
from stax.utils.utils_timeline import (
    add_overlay_to_sequence,
    set_reviewed_state,
    display_media_annotations,
    get_media_sequence,
    get_parent_metas,
    get_source_media_path,
    set_preview_range_from_sequences,
)


class OpenWorkFolder(bpy.types.Operator):
    """Open current shot's work folder"""

    bl_idname = "wm.open_work_folder"
    bl_label = "Open Work Folder"

    def execute(self, context):
        clip = utils_timeline.get_current_clip()
        if not clip:
            self.report(
                {"ERROR"},
                f"no clip on channel: {context.space_data.display_channel}",
            )
            return {"FINISHED"}

        # Open server folder where working files (mesh, scene...) are.
        work_folder = utils_linker.get_linker().get_work_folder(clip)

        if work_folder.is_dir():
            bpy.ops.wm.path_open(filepath=str(work_folder))
        else:
            self.report({"ERROR"}, f"folder missing: {work_folder}")
        return {"FINISHED"}


class WM_OT_OpenMediaInBrowser(bpy.types.Operator):
    """Open current shot's web page"""

    bl_idname = "wm.open_media_in_browser"
    bl_label = "Open in browser"

    def invoke(self, context, event):
        clip = utils_timeline.get_current_clip()

        if not clip:
            self.report(
                {"ERROR"}, f"no clip on channel: {context.space_data.display_channel}"
            )
            return {"FINISHED"}

        # Open web page for given sequence
        webpage = utils_linker.get_linker().get_webpage_url(clip)
        from webbrowser import open as openwb

        openwb(webpage)

        return {"FINISHED"}


class WM_OT_SelectExternalImageEditor(bpy.types.Operator, ImportHelper):
    """Select an external image editor through the file browser"""

    bl_idname = "wm.select_external_image_editor"
    bl_label = "Select an external image editor"

    filepath: bpy.props.StringProperty(
        name="Executable path",
        subtype="FILE_PATH",
    )

    def execute(self, context):
        # Set the path to the image editor Blender's path
        bpy.context.preferences.filepaths.image_editor = self.filepath

        # Save the preferences
        bpy.ops.wm.save_userpref()

        # Notify the user
        editor_name = Path(self.filepath).stem
        bpy.ops.wm.report_message(
            type="INFO",
            message=f"{editor_name} has been set as External Editor. Click again on the button to edit the current frame.",
        )

        return {"FINISHED"}


class WM_OT_edit_image_externally(bpy.types.Operator):
    """Edit current image with external image editor"""

    bl_idname = "wm.edit_in_image_editor"
    bl_label = "Edit in external image editor"
    bl_options = {"REGISTER", "UNDO"}

    edited_image = None

    @classmethod
    def poll(cls, context):
        if bpy.data.scenes["Scene"].sequence_editor.active_strip:
            return True

    def execute(self, context):
        scene = context.scene
        wm = context.window_manager

        # Get the sequence to edit the image
        media_sequence = get_media_sequence(scene.sequence_editor.active_strip)

        if not media_sequence:
            self.report({"ERROR"}, f"no clip on track: {scene.current_displayed_track}")
            return {"FINISHED"}

        #   Export frame and save
        # Set image format
        scene.render.image_settings.file_format = "JPEG"
        scene.render.image_settings.color_mode = "RGB"
        scene.render.image_settings.quality = 90
        scene.view_settings.view_transform = "Standard"

        # Hide annotations drawings
        context.space_data.show_annotation = False

        # Hide advanced drawings
        display_media_annotations(media_sequence, False)

        # Disable temporarily emphasize drawings
        keep_emphasize_drawings = wm.emphasize_drawings
        wm.emphasize_drawings = 0

        # Export
        image_path_base = utils_cache.get_temp_directory().joinpath("ext_edited")
        frame_current = scene.frame_current

        image_name = f"external_annotation_{frame_current}"
        image_path = image_path_base.joinpath(image_name).with_suffix(
            "." + scene.render.image_settings.file_format.lower()
        )

        scene.render.filepath = str(image_path)
        bpy.ops.render.opengl(animation=False, sequencer=True, write_still=True)

        # Show annotations drawings
        context.space_data.show_annotation = True

        # Show advanced drawings
        display_media_annotations(media_sequence, True)

        # Restore emhasize drawings value
        wm.emphasize_drawings = keep_emphasize_drawings

        # Open image in external editing software
        bpy.ops.image.open(filepath=str(image_path))
        bpy.ops.image.external_edit(filepath=str(image_path))

        # Dynamic function
        def add_externally_edited_image(_) -> bpy.types.ImageSequence:
            edited_image = scene.sequence_editor.sequences.new_image(
                name=image_path.name,
                filepath=str(image_path),
                frame_start=frame_current,
                channel=media_sequence.channel,
            )

            edited_image.blend_type = "CROSS"
            edited_image["kind"] = "externally_edited"

            return edited_image

        self.edited_image = add_overlay_to_sequence(
            media_sequence, add_externally_edited_image
        )

        # Refresh sequencer
        bpy.ops.sequencer.refresh_all()

        # Associate the image to the sequence
        if not media_sequence.get("external_images"):
            media_sequence["external_images"] = []
        media_sequence["external_images"] = list(media_sequence["external_images"]) + [
            self.edited_image.name
        ]

        # Automatically start review
        context.scene.review_session_active = True

        # Set reviewed state to sequence
        set_reviewed_state(media_sequence)

        # Update image
        context.window_manager.modal_handler_add(self)
        return {"RUNNING_MODAL"}

    def modal(self, context, event):
        # Every time the user clicks, every externally edited images paths are reloaded to refresh edited image.
        # It could be optimized but we'll wait for users complain because the lack of reactivity is almost invisible.

        # If the image is deleted, cancel
        if not self.edited_image:
            return {"CANCELLED"}

        # Reload the image when clicking in the interface
        if event.type == "LEFTMOUSE":
            self.edited_image.directory = self.edited_image.directory

        # Finish if the review session is stopped
        if not context.scene.review_session_active:
            return {"FINISHED"}

        return {"PASS_THROUGH"}


class WM_OT_advanced_drawing(bpy.types.Operator):
    """Toggle review in advanced drawing mode"""

    bl_idname = "wm.advanced_drawing"
    bl_label = "Advanced Drawing"
    bl_options = {"UNDO_GROUPED"}

    start: bpy.props.BoolProperty(default=True, options={"SKIP_SAVE"})
    cancel: bpy.props.BoolProperty(options={"SKIP_SAVE"})

    @classmethod
    def poll(cls, context):
        current_sequence = bpy.data.scenes["Scene"].sequence_editor.active_strip
        if current_sequence and not current_sequence.lock:
            return True

    def execute(self, context):
        scene = context.scene
        data = bpy.data
        wm = context.window_manager

        # Tell the impatient user to wait
        context.window.cursor_set("WAIT")

        # Get current sequence
        current_sequence = data.scenes["Scene"].sequence_editor.active_strip
        if not current_sequence:
            bpy.ops.wm.report_message(type="WARNING", message="No media to review")
            return {"CANCELLED"}

        # Get media sequence
        media_sequence = get_media_sequence(current_sequence)

        # Set parameters
        media_path = get_source_media_path(media_sequence)
        gp_name = f"drawing.{media_path.name}"

        # Cancel
        if self.cancel:
            # Set source GP to main object
            data.objects["GPencil"].data = data.grease_pencils["GPencil"]

            # If drawing scene associated to sequence, don't delete it,
            # it means it has been confirmed once and "cancel" musn't delete it.
            # In fact, only delete if no drawing scene has been associated to the reviewed sequence
            if not media_sequence.get("drawing_scene", None):
                # Delete created GP
                gp = data.grease_pencils.get(gp_name)
                data.grease_pencils.remove(gp)

                # Delete created scene
                data.scenes.remove(scene)
            else:  # Restore scene previous state of the grease pencil
                # There is a restore_gp_data linked to the drawing scene, it's copied and replaces the
                # current active one and is associated to the sequence as its main gp object
                old_gp_data = media_sequence.get("gp")

                # Copy the backup one as the main one
                restored_gp_data = scene.get("restore_gp_data").copy()

                # Remove the '#restore' extension in the name
                restored_gp_data.name = restored_gp_data.name.split("#")[0]

                # Set the restored to the grease pencil object
                context.active_object.data = restored_gp_data

                # Set the restored scene to the current sequence
                media_sequence["gp"] = restored_gp_data

                # Delete old drawing scene grease pencil data
                data.grease_pencils.remove(old_gp_data)

            # Exit mode
            self.exit_advanced_drawing_ui(context, data)

            # Update emphasize drawings
            wm.emphasize_drawings = wm.emphasize_drawings

            # Save
            bpy.ops.wm.save_mainfile()

            return {"FINISHED"}

        if self.start:  # Open Advanced Drawing
            # Switch to drawing scene
            drawing_scene = media_sequence.get("drawing_scene", None)
            if not drawing_scene:  # Create new drawing scene
                bpy.ops.scene.new(type="LINK_COPY")
                drawing_scene = context.scene
                drawing_scene.name = f"{media_path.name}_drawing"
                # NB: No need to set the new scene as active because scene.new() does it
            else:  # Set the existing drawing scene as active
                context.window.scene = drawing_scene

            # Switch to Drawing workspace
            context.window.workspace = data.workspaces["Advanced drawing"]

            # Set scene BG as transparent
            drawing_scene.render.film_transparent = True

            # #####
            # Set GPencil
            # #####

            # Get GP if sequence has one, else create one
            gp = media_sequence.get("gp", None)
            if gp is None:
                # Copy source GPencil
                gp = data.grease_pencils["GPencil"].copy()
                gp.name = gp_name
                # Set the new one to the GP object
                data.objects["GPencil"].data = gp

                # Associate it to the source sequence
                media_sequence["gp"] = gp
            else:
                # Set the associated GP to the GP object
                data.objects["GPencil"].data = gp

            # Settings
            # -------

            # Set vertex mode to FILL and STROKE
            drawing_scene.tool_settings.gpencil_paint.brush.gpencil_settings.vertex_mode = (
                "BOTH"
            )

            # Set pencil settings
            # Saving these data in the startup.blend doesn't work if the startup file
            # is loaded from the stax app template dir. Because it's a user configuration. TODO: is this a bug?
            drawing_scene.tool_settings.gpencil_paint.brush.gpencil_settings.pen_strength = (
                1.0
            )
            data.brushes["Pencil"].size = 8
            # -------

            # Empty Frame to start and end
            for layer in gp.layers:
                if not len(layer.frames):  # If not already created
                    # -/+ 1 to have empty keyframes out of range to avoid user touching them
                    # and drawings lasting from beginning
                    layer.frames.new(media_sequence.frame_final_start - 1)
                    layer.frames.new(media_sequence.frame_final_end + 1)

            # #####
            # Set current sequence as background clip
            # #####

            # Set as background image
            bg_img = data.images["Background"]
            bg_img.filepath = str(media_path)
            bg_empty = data.objects["Background"]

            # Set image time
            bg_empty.image_user.frame_start = media_sequence.frame_start
            bg_empty.image_user.frame_duration = media_sequence.frame_final_duration

            # Set alpha to emphasize drawing value
            bg_empty.use_empty_image_alpha = True
            bg_empty.color[3] = (100 - context.window_manager.emphasize_drawings) / 100

            # Add sound by adding main scene as sequence
            # ----------------------
            adv_sequence_editor = drawing_scene.sequence_editor_create()
            main_scene = data.scenes["Scene"]
            scene_seq = adv_sequence_editor.sequences.new_scene(
                name="Source Scene",
                scene=main_scene,
                channel=1,
                frame_start=main_scene.frame_start,
            )
            scene_seq.scene_input = "SEQUENCER"
            # ----------------------

            # Center the keyframes view
            # ---------------
            # Override context
            override = get_context("Advanced drawing", "GPENCIL")
            bpy.ops.action.view_all(override)

            # Set scene range to sequence
            drawing_scene.frame_start = media_sequence.frame_final_start
            drawing_scene.frame_end = media_sequence.frame_final_end

        else:  # Complete drawing process, Render and go back to Main view

            # Set drawing scene to sequence to keep it
            media_sequence["drawing_scene"] = scene

            # #####
            # Render drawings
            # TODO when the GPencil from a scene strip is stable and fast enough, refactor this part
            # #####
            annotations_directory = Path(
                utils_cache.get_temp_directory(), "annotations"
            )
            rendered_frames = []  # List of tuples (path_to_image, start, duration)

            # Clean previous rendered images references if needed
            media_sequence["rendered_images"] = []

            # Set render settings TODO make it a function, duplicated from PublishSession
            scene.render.image_settings.file_format = "PNG"
            scene.render.image_settings.color_mode = "RGBA"
            scene.render.image_settings.compression = 70

            # Disable onion skins to avoid ghosts
            space_data = data.screens["Advanced drawing"].areas[1].spaces[0]
            space_data.overlay.use_gpencil_onion_skin = False

            # Hide BG white image
            camera = data.cameras["Camera"]
            camera.show_background_images = False

            # Hide background in viewport
            bg_empty = data.objects["Background"]
            bg_empty.hide_viewport = True

            # Disable temporarily emphasize drawings
            keep_emphasize_drawings = wm.emphasize_drawings
            wm.emphasize_drawings = 0

            #  Hide all layers
            gp = media_sequence.get("gp")
            for layer in gp.layers:
                layer.hide = True

            for layer in gp.layers:
                layer.hide = False  # Unhide layer to see it

                all_frames = list(layer.frames)
                for i, frame in enumerate(all_frames):
                    if frame.frame_number < media_sequence.frame_final_end:
                        # Move playhead to frame
                        scene.frame_set(frame.frame_number)

                        # Calculate duration. If last GP frame use end frame
                        annotation_duration = (
                            all_frames[i + 1].frame_number - frame.frame_number
                        )

                        # Render only if frame has drawings
                        if len(frame.strokes):
                            # Build image name and path
                            annotation_name = (
                                f"annotation_{layer.info}_{frame.frame_number}"
                            )
                            annotation_path = annotations_directory.joinpath(
                                annotation_name
                            ).with_suffix(
                                "." + scene.render.image_settings.file_format.lower()
                            )

                            # Set scene settings for render
                            scene.render.filepath = str(annotation_path)

                            # Export

                            # Override context if operator not run from Drawing workspace
                            override = get_context(
                                "Advanced drawing", "VIEW_3D", use_current_scene=True
                            )
                            bpy.ops.render.opengl(override, write_still=True)

                            # Keep the rendered image
                            rendered_frames.append(
                                (
                                    annotation_path,
                                    frame.frame_number,
                                    annotation_duration,
                                )
                            )

                layer.hide = True  # Hide layer back

            # Show BG white image
            camera.show_background_images = True

            # Show back background
            bg_empty.hide_viewport = False

            # Restore emhasize drawings value
            wm.emphasize_drawings = keep_emphasize_drawings

            # Unhide all layers
            for layer in gp.layers:
                layer.hide = False

            # Set onion skins back
            space_data.overlay.use_gpencil_onion_skin = True

            # GP for restoring
            # --------
            current_gp_data = context.active_object.data

            restore_gp_data = scene.get("restore_gp_data", None)
            if restore_gp_data:  # Clean it if exists
                bpy.data.grease_pencils.remove(restore_gp_data)

            # Create a whole new one from the current one
            restore_gp_data = current_gp_data.copy()
            restore_gp_data.name = f"{current_gp_data.name}#restore"
            scene["restore_gp_data"] = restore_gp_data
            # ---------

            # #####
            # Back to SEQUENCER
            # #####

            # Set source GP to main object
            data.objects["GPencil"].data = data.grease_pencils["GPencil"]

            self.exit_advanced_drawing_ui(context, data)

            # Add overlay to sequence
            # -----------------------

            # Clean existing overlay
            if type(current_sequence) is bpy.types.MetaSequence:
                all_metas = get_parent_metas(media_sequence)
                parent_meta = all_metas[-1]

                # Open meta hierarchy
                # TODO gonna be removed with 2.92
                for meta in all_metas:
                    meta.select = True
                    data.scenes["Scene"].sequence_editor.active_strip = meta

                    # Open meta
                    bpy.ops.sequencer.meta_toggle(get_context("Main", "SEQUENCER"))

                # Delete existing overlay
                bpy.ops.sequencer.select_all(
                    get_context("Main", "SEQUENCER"), action="DESELECT"
                )
                for seq in parent_meta.sequences:
                    if seq.name.startswith("drawings."):
                        seq.select = True
                    else:
                        seq.select = False
                bpy.ops.sequencer.delete(get_context("Main", "SEQUENCER"))

                # Close meta hierarchy
                # TODO gonna be removed with 2.92
                for meta in all_metas:
                    # Deselect to avoid entering in a newly created meta
                    bpy.ops.sequencer.select_all(action="DESELECT")

                    scene.sequence_editor.active_strip = None

                    bpy.ops.sequencer.meta_toggle()  # Close meta

            # Create overlay
            def add_rendered_frames_as_overlay(_) -> bpy.types.MetaSequence:
                """Create rendered frames as images sequences for overlay"""
                created_images = []
                for i, render in enumerate(rendered_frames):
                    render_path, render_frame_start, render_duration = render
                    image = data.scenes["Scene"].sequence_editor.sequences.new_image(
                        name=render_path.name,
                        filepath=str(render_path),
                        channel=media_sequence.channel + i,
                        frame_start=render_frame_start,
                    )
                    if image:
                        image.frame_final_duration = render_duration
                        image.blend_type = "ALPHA_OVER"
                        image["kind"] = "annotation"
                        created_images.append(image)

                # Create metasequence
                # Override context for sequencer
                bpy.ops.sequencer.select_all(
                    {"scene": bpy.data.scenes["Scene"]}, action="DESELECT"
                )
                for img in created_images:
                    img.select = True

                bpy.ops.sequencer.meta_make()

                # Set parameters of metasequence
                meta = data.scenes["Scene"].sequence_editor.active_strip
                meta.blend_type = "ALPHA_OVER"
                meta.name = f"drawings.{media_sequence.name}"

                meta["kind"] = "temp"
                # ^ Because trick meta as a buffer for optim before actual GP in sequencer

                # Set created images names to sequence
                media_sequence["rendered_images"] = [img.name for img in created_images]

                return meta

            if rendered_frames:
                add_overlay_to_sequence(media_sequence, add_rendered_frames_as_overlay)

            # Update emphasize drawings
            wm.emphasize_drawings = wm.emphasize_drawings

            # Consider review session as active
            context.scene.review_session_active = True

            # Set as reviewed
            set_reviewed_state(media_sequence)

        # Save
        bpy.ops.wm.save_mainfile()

        return {"FINISHED"}

    def exit_advanced_drawing_ui(self, context, data):
        """Return to sequence editor Main view"""
        # Set default scene back
        context.window.scene = data.scenes["Scene"]

        # Set Main workspace
        context.window.workspace = data.workspaces["Main"]


class AddEmptyGPFrame(bpy.types.Operator):
    """Add an empty frame for the current GPencil layer"""

    bl_idname = "scene.add_empty_gp_frame"
    bl_label = "Empty frame"
    bl_options = {"REGISTER", "UNDO"}

    def execute(self, context):
        scene = context.scene

        # Select the active gpencil
        if scene is bpy.data.scenes["Scene"]:
            gp = context.annotation_data
        else:
            gp = context.gpencil_data

        grease_pencil_layer_frames = gp.layers.active.frames

        # Test if there is already a frame and delete it
        for frame in grease_pencil_layer_frames:
            if frame.frame_number == context.scene.frame_current:
                grease_pencil_layer_frames.remove(frame)

        # Create keyframe for every layer
        frame = grease_pencil_layer_frames.new(context.scene.frame_current)

        # Hack to refresh the drawing... TODO report that to Blender
        scene.fill_opacity = scene.fill_opacity
        return {"FINISHED"}


class SCENE_OT_clear_drawings(bpy.types.Operator):
    """Clear all drawings of the current displayed sequence"""

    bl_idname = "scene.clear_drawings"
    bl_label = "Clear Drawings"

    @classmethod
    def poll(cls, context):
        media_sequence = get_media_sequence(context.scene.sequence_editor.active_strip)

        if media_sequence and bpy.data.scenes["Scene"].review_session_active:
            # Test if at least one annotate drawing is over the sequence
            for layer in bpy.data.grease_pencils["Annotations"].layers:
                for frame in layer.frames:
                    if (
                        media_sequence.frame_start
                        <= frame.frame_number
                        < media_sequence.frame_final_end
                        and frame.strokes
                    ):
                        return True

            # Test if rendered drawing images are associated to sequence
            all_metas = get_parent_metas(media_sequence)
            if all_metas:
                parent_meta = all_metas[-1]
                for seq in parent_meta.sequences:
                    # Test if any rendered image (adv. drawing or external) is associated to the sequence
                    if seq.get("kind") in {"temp", "externally_edited"}:
                        return True

    def execute(self, context):
        sequence_editor = context.scene.sequence_editor
        data = bpy.data
        media_sequence = get_media_sequence(sequence_editor.active_strip)

        bpy.ops.sequencer.select_all(action="DESELECT")

        # Clear Advanced Drawings
        # ----------------------
        clear_advanced_drawings(media_sequence)

        # Annotations
        # -----------
        clear_annotate(media_sequence)

        # Create the empty drawing at start back to stop drawings at each shot
        data.grease_pencils["Annotations"].layers["Note"].frames.new(
            media_sequence.frame_start
        )

        # Check if is reviewed
        set_reviewed_state(media_sequence)

        # Refresh sequencer
        bpy.ops.sequencer.refresh_all()

        return {"FINISHED"}


class SEQUENCER_OT_ToggleVersions(bpy.types.Operator):
    """Toggle versions of the displayed element"""

    bl_idname = "sequencer.toggle_versions"
    bl_label = "Toggle Versions"

    @classmethod
    def poll(cls, context):
        sequence_editor = context.scene.sequence_editor
        active_strip = sequence_editor.active_strip

        if (
            sequence_editor.sequences
            and active_strip
            and active_strip.get("has_versions", False)
        ):
            return True

    def execute(self, context):
        scene = context.scene

        if not scene.versions_displayed_sequence:  # Show versions
            # Get sequence
            current_sequence = scene.sequence_editor.active_strip
            current_track_index = scene.tracks.find(scene.current_displayed_track)
            # Get the related element (used for versions)
            for track in scene.tracks[: current_track_index + 1]:
                for elem in track.elements:
                    if elem.name == current_sequence.get("element_name"):
                        element = elem
                        break

            current_track_index = current_sequence.channel - 1
            versions = element.versions

            # If no version to show, abort and notify user
            if len(versions) < 2:
                self.report(
                    {"INFO"},
                    "Only one version for this element. No more version to show.",
                )
                return {"CANCELLED"}

            # Get previous versions
            previous_versions = list(versions)[:-1]

            # Set timeline preview range to current sequence boundaries
            set_preview_range_from_sequences([current_sequence])

            # Select overlapping sequences
            selected_sequences = utils_timeline.select_overlapping_sequences(
                current_sequence
            )
            # Unlock them to be able to move them
            bpy.ops.sequencer.unlock()
            # Move above sequences
            for seq in [
                s for s in selected_sequences if s.channel < current_sequence.channel
            ]:
                seq.select = False
            bpy.ops.transform.seq_slide(
                get_context("Main", "SEQUENCER"), value=(0, len(previous_versions))
            )

            # Insert versions
            previous_versions_count = len(previous_versions)
            for i, version in enumerate(previous_versions):
                # Insert versions in displayed tracks
                version_track = scene.tracks.add()
                version_track.name = f"version: {- (previous_versions_count - i)}"  # TODO: set a way to display the absolute version: get from otio?
                version_track.temp = True

                # Move created version track to correct index
                scene.tracks.move(
                    scene.tracks.find(version_track.name), current_track_index
                )

                # Create version sequence
                seq = element.display(i, current_track_index + 1)
                seq.select = True
                seq["kind"] = "version"

                current_track_index += 1

            # Display annotations for created sequences
            bpy.ops.sequencer.show_sequences_annotations()

            # Change displayed track
            scene.current_displayed_track = get_displayed_tracks()[
                current_sequence.channel - 1
            ]

            # Set keeper
            scene.versions_displayed_sequence = current_sequence.name

        else:  # Hide versions
            current_sequence = scene.sequence_editor.sequences.get(
                scene.versions_displayed_sequence
            )

            # Delete versions sequences
            #   Select overlaping sequences
            selected_sequences = utils_timeline.select_overlapping_sequences(
                current_sequence
            )

            #   Delete 'version' kind sequences
            #   Keep above sequences
            versions_count = 0
            above_sequences = []
            for seq in selected_sequences:
                if seq.get("kind", "") == "version":
                    scene.sequence_editor.sequences.remove(seq)
                    versions_count += 1
                else:
                    seq.select = False
                    if seq.channel >= current_sequence.channel:
                        above_sequences.append(seq)

            # Remove temporary versions tracks. Do not use track.delete() to not move sequences from above tracks
            for i, track in reversed(list(enumerate(scene.tracks))):
                if track.temp:
                    scene.tracks.remove(i)

            # Move back main and above sequences
            for seq in above_sequences:
                seq.select = True
            # Unlock them to be able to move them
            bpy.ops.sequencer.unlock()

            bpy.ops.transform.seq_slide(
                get_context("Main", "SEQUENCER"), value=(0, -(versions_count))
            )

            # Change displayed track
            scene.current_displayed_track = get_displayed_tracks()[
                current_sequence.channel - 1
            ]

            # Set keeper to default and stop preview range
            scene.versions_displayed_sequence = ""
            scene.use_preview_range = False

        # Lock back review disabled media
        bpy.ops.sequencer.lock_review()

        # Highlight updated reviews
        bpy.ops.sequencer.highlight_updated_reviews({"scene": bpy.data.scenes["Scene"]})

        return {"FINISHED"}


class WM_OT_stax_layer_annotation_add(bpy.types.Operator):
    """Enhances the native gpencil.layer_annotation_add to add empty keyframes to every start/end of medias"""

    bl_idname = "gpencil.stax_layer_annotation_add"
    bl_label = "Add Annotation Layer"

    @classmethod
    def poll(cls, context):
        if context.scene.sequence_editor.sequences:
            return True

    def execute(self, context):
        bpy.ops.gpencil.layer_annotation_add()

        # Add empty keyframes to all medias boundaries of the current layer
        gpd = context.annotation_data
        created_layer = gpd.layers[-1]

        all_sequences = context.scene.sequence_editor.sequences
        last_sequence = all_sequences[-1]
        cut_keyframes = []  # Keep the created keyframes to avoid error
        for seq in all_sequences:
            if seq.frame_start not in cut_keyframes:
                created_layer.frames.new(seq.frame_start)
            cut_keyframes.append(seq.frame_start)

            # Get the latest sequence horizontally
            if seq.frame_final_end > last_sequence.frame_final_end:
                last_sequence = seq

        # Add the last empty frame
        created_layer.frames.new(last_sequence.frame_final_end + 1)

        return {"FINISHED"}


class WM_OT_stax_layer_add(bpy.types.Operator):
    """Enhances the native gpencil.layer_add to add empty keyframes to the current reviewed media"""

    bl_idname = "gpencil.stax_layer_add"
    bl_label = "Add Layer"

    @classmethod
    def poll(cls, context):
        if context.scene is not bpy.data.scenes["Scene"]:
            return True

    def execute(self, context):
        current_sequence = bpy.data.scenes["Scene"].sequence_editor.active_strip
        gpd = context.gpencil_data

        # Create new layer
        bpy.ops.gpencil.layer_add()
        created_layer = gpd.layers[-1]

        # Add empty keyframes to all medias boundaries of the current layer
        created_layer.frames.new(current_sequence.frame_start - 1)
        created_layer.frames.new(current_sequence.frame_final_end + 1)

        return {"FINISHED"}


classes = [
    OpenWorkFolder,
    WM_OT_OpenMediaInBrowser,
    WM_OT_SelectExternalImageEditor,
    WM_OT_edit_image_externally,
    WM_OT_advanced_drawing,
    AddEmptyGPFrame,
    SCENE_OT_clear_drawings,
    SEQUENCER_OT_ToggleVersions,
    WM_OT_stax_layer_annotation_add,
    WM_OT_stax_layer_add,
]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)


def unregister():
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
