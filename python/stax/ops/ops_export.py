# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""
Every operator relative to the export tool
"""

import bpy
from math import sin, cos, pi

from stax.utils.utils_timeline import update_tracks


class NewRenderTrack(bpy.types.Operator):
    """Add a new track to be rendered"""

    bl_idname = "scene.new_render_track"
    bl_label = "Add new render track"
    bl_options = {"REGISTER", "UNDO"}

    def execute(self, context):
        scene = context.scene

        # Add track to render tracks
        scene.render_tracks.add()

        return {"FINISHED"}


class RemoveRenderTrack(bpy.types.Operator):
    """Remove a track in render tracks list"""

    bl_idname = "scene.remove_render_track"
    bl_label = "Remove render track"
    bl_options = {"REGISTER", "UNDO"}

    name: bpy.props.StringProperty(name="Track name", subtype="FILE_NAME")

    def execute(self, context):
        scene = context.scene

        track_index = scene.render_tracks.find(self.name)
        scene.render_tracks.remove(track_index)

        return {"FINISHED"}


class ExportRender(bpy.types.Operator):
    """Render"""

    bl_idname = "scene.export_render"
    bl_label = "Render sequence"

    def execute(self, context):
        scene = context.scene

        # Display loading for user
        context.window.cursor_set("WAIT")

        # Clean sequencer
        bpy.ops.sequencer.select_all(action="SELECT")
        bpy.ops.sequencer.delete()

        #       Create strips
        #   Sound, if a track 'Sound' is found, it'll be automatically added
        sound_track = scene.tracks.get("Sound")
        if sound_track:
            for sound in sound_track.elements:
                meta_sound = sound.display(channel_index=len(scene.render_tracks) + 1)
                if meta_sound:
                    meta_sound.blend_type = "ALPHA_OVER"

            # Deselect
            bpy.ops.sequencer.select_all(action="DESELECT")

        #    Movies
        rotation = 2 * pi / len(scene.render_tracks)

        for i, render_track in enumerate(scene.render_tracks):
            bpy.ops.sequencer.select_all(action="DESELECT")

            for element in scene.tracks.get(render_track.name).elements:
                if element.render:
                    element.display(channel_index=i + 1)

            # Group strips of the current track if strips created. If no strip at all, don't render and reload tracks
            # =====

            # Select all task sequences
            bpy.ops.sequencer.select_side(side="BOTH")

            try:
                bpy.ops.sequencer.meta_make()
            except RuntimeError:
                self.report(
                    {"ERROR"},
                    f"{render_track.name} cannot find any clip to render. Please check their filepaths.",
                )
                if len(scene.render_tracks) == 1:
                    self.clean_timeline()
                    return {"CANCELLED"}
            seq = scene.sequence_editor.active_strip

            # Add transform strip and set parameters
            transform = scene.sequence_editor.sequences.new_effect(
                name="",
                type="TRANSFORM",
                channel=seq.channel + 1,
                frame_start=0,
                seq1=seq,
            )
            transform.blend_type = "ALPHA_OVER"
            transform.use_uniform_scale = True

            # Tracks meta strip transform
            if len(scene.render_tracks) > 1:
                transform.scale_start_x = 0.5 - 0.05
                if len(scene.render_tracks) == 2:
                    translate_x_sign = 1 if cos(rotation * -i) < 0 else -1
                    transform.translate_start_x = translate_x_sign * 25
                    transform.translate_start_y = 0
                elif len(scene.render_tracks) == 3:
                    translate_x_sign = 1 if cos(rotation * -i) < 0 else -1
                    translate_y_sign = 1 if sin(rotation * -i) < 0 else -1
                    transform.translate_start_x = translate_x_sign * 25
                    transform.translate_start_y = translate_y_sign * 25 if i != 0 else 0
                elif len(scene.render_tracks) == 4:
                    translate_x_sign = 1 if cos(rotation * -i) < 0 else -1
                    translate_y_sign = 1 if sin(rotation * -i) < 0 else -1
                    transform.translate_start_x = translate_x_sign * 25
                    transform.translate_start_y = translate_y_sign * 25
            else:
                transform.scale_start_x = 1

            # Add text strip
            text = scene.sequence_editor.sequences.new_effect(
                name="",
                type="TEXT",
                channel=transform.channel + 1,
                frame_start=0,
                frame_end=scene.frame_end,
            )
            text.text = "".join(
                [
                    render_track.name,
                    " -- " if render_track.comment else "",
                    render_track.comment,
                ]
            )
            text.font_size = 20
            text.align_y = "TOP"
            text.location = [
                0.5 + transform.translate_start_x / 100,
                0.05
                if len(scene.render_tracks) == 1
                else 0.28 + transform.translate_start_y / 100,
            ]
            text.blend_type = "ALPHA_OVER"

            # Group sequences
            bpy.ops.sequencer.meta_make()
            scene.sequence_editor.active_strip.blend_type = "ALPHA_OVER"

        # Start render
        bpy.ops.render.render(animation=True)

        # Clean sequencer and reload tracks
        self.clean_timeline()

        self.report({"INFO"}, "Export render finished")

        return {"FINISHED"}

    @staticmethod
    def clean_timeline():
        """Clean the timeline and set back tracks"""
        # Set element.render to default
        for track in bpy.context.scene.tracks:
            for element in track.elements:
                element.render = True

        # Clean timeline and reload tracks
        bpy.ops.sequencer.select_all(action="SELECT")
        bpy.ops.sequencer.delete()
        update_tracks()


classes = [NewRenderTrack, RemoveRenderTrack, ExportRender]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)


def unregister():
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
