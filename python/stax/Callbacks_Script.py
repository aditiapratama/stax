# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""Callbacks"""

from typing import Callable, Tuple, Set

import bpy
from openreviewio import MediaReview, Note


class Callbacks(bpy.types.PropertyGroup):
    """Parent class for callbacks scripts

    :param name: Name of the callback
    :param description: Description of the callback
    """

    name: bpy.props.StringProperty(default="No name provided for callbacks script")
    description: str = "No description provided for callbacks script"

    def authentication_exec(login: str, password: str) -> bool:
        """Run authentication, when the 'OK' button is clicked in the popup window

        Corresponds to the operator's execute() call.

        :param login: User Login
        :param password: User Password
        :return: Boolean for authentication success or failure
        """
        return NotImplementedError

    def session_update_pre(timeline_path: str, reviews_folder=""):
        """Before session's update

        Allows to update the timeline file and the reviews before loading it in Stax.
        Update is included in session's loading.

        :param timeline_path: Path to source timeline
        :param reviews_folder: Path to reviews folder, default to ""
        """
        return NotImplementedError

    def review_session_publish(edited_notes: Set[Tuple[MediaReview, Note]]):
        """When the review session is published

        Versions and reviews lists order match.

        :param edited_notes: All edited notes and their source reviews.
        """
        return NotImplementedError

    def get_work_folder(self, sequence):
        """Get the work folder of the current sequence

        TODO will be deleted

        :param sequence: Current sequence
        :return: Work folder as Path
        """
        raise NotImplementedError

    def get_webpage_url(self, element_name):
        """Return the url in the Production Tracker website of the given element name

        TODO will be deleted

        :param element_name: Element name as string
        :return: URL as string
        """
        raise NotImplementedError
