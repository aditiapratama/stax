# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""
Every class related to core architecture design
"""
from datetime import datetime
import fractions
import getpass
import inspect
from pathlib import Path
from pymediainfo import MediaInfo
from re import sub
from typing import Union

import bpy
import openreviewio as orio
from bpy.app.handlers import persistent

from stax.utils import utils_cache, utils_core, utils_linker
from stax.utils.utils_timeline import (
    add_wipe_effect,
    frame_change_update,
    get_sequence_in_list,
    get_wipe_sequence,
    get_parent_metas,
    make_meta,
    set_sequence_attributes,
    update_preview_channel,
)
from stax.utils.utils_openreviewio import available_statuses_callback


@persistent
def update_viewer_displayed_track(self, context):
    """Update displayed track

    Pass by function.

    :param context: Current context"""
    update_preview_channel()


def current_displayed_track_items_callback(screen, context):
    """Get items for enum property from track

    :param screen: Blender current screen object
    :param context: Blender context
    """
    displayed_tracks = utils_core.get_displayed_tracks()
    return [
        (track, track, "No description") for track in displayed_tracks
    ]  # TODO: maybe allow description from linker, like it is for timelines


@persistent
def update_review_status(self, context):
    """Update the sequence status based on the related review"""
    sequence = context.scene.sequence_editor.sequences_all.get(self.sequence_name)
    if sequence:
        sequence["status"] = self.status


class Version(bpy.types.PropertyGroup):
    """Version of an element"""

    name: bpy.props.StringProperty(
        name="Version Name",
        description="Name for blender collection storage, must be unique and constant",
    )
    frame_start: bpy.props.IntProperty(name="Track element start frame")
    clip_offset_start: bpy.props.IntProperty(
        name="Start offset",
        description="Clip start frame offset (cut frames at start of clip)",
        default=0,
    )
    duration: bpy.props.IntProperty(name="Track element duration")
    filepath: bpy.props.StringProperty(name="Media path", subtype="FILE_PATH")
    cache_filepath: bpy.props.StringProperty(
        name="Cached media filepath", subtype="FILE_PATH"
    )
    include_sound: bpy.props.BoolProperty(
        name="Include Sound",
        description="If True, try to join the displayed version's sound track into a metastrip.",
        default=False,
    )
    preserve_ratio: bpy.props.BoolProperty(
        name="Preserve Ratio",
        description="If True, Stax will get the ratio from the media file and preserve it. This slows down timeline loading.",
        default=False,
    )


class Element(bpy.types.PropertyGroup):
    """Element of a track (asset, shot...)"""

    name: bpy.props.StringProperty(
        name="Track Element name",
        description="Name for blender collection storage, must be unique and constant",
    )
    versions: bpy.props.CollectionProperty(name="Versions", type=Version)
    render: bpy.props.BoolProperty(name="To render in Export", default=True)

    def display(
        self, version_index: int = -1, channel_index: int = 1
    ) -> bpy.types.MovieSequence:
        """Display the element in the sequencer

        :param version_index: Index of the version to display
        :param channel_index: Channel index to display the element on
        :return: Created sequence
        """
        if self.versions:
            scene = bpy.data.scenes["Scene"]
            scene_sequences = scene.sequence_editor.sequences

            version = self.versions[version_index]
            include_sound = version.include_sound

            # Get sequence of version
            sequence = get_sequence_in_list(version.name, scene_sequences)
            audio_strip = None

            if sequence:
                # Move to appropriate channel
                sequence.channel = channel_index

            else:
                # Set source media file for version
                media_file = (
                    Path(version.filepath).resolve()
                    if not version.cache_filepath
                    else Path(version.cache_filepath).resolve()
                )

                is_audio = (
                    media_file.suffix.lower() in utils_core.get_audio_extensions()
                )
                is_video = (
                    media_file.suffix.lower() in utils_core.get_video_extensions()
                )
                is_image = (
                    media_file.suffix.lower() in utils_core.get_image_extensions()
                )

                # Test if file exists
                if not media_file.is_file():
                    utils_core.report_message({"WARNING"}, f"missing file: {self.name}")

                # Case of audio file
                elif is_audio:
                    try:
                        sequence = scene.sequence_editor.sequences.new_sound(
                            name=version.name,
                            filepath=str(media_file),
                            channel=channel_index,
                            frame_start=version.frame_start,
                        )
                        sequence.show_waveform = True
                    except RuntimeError as e:
                        print(e, version.filepath)

                # Case of video file, without the instruction to also include the sound track
                # (note that still images are handled as if video files)
                elif is_video and not include_sound:
                    try:
                        sequence = scene.sequence_editor.sequences.new_movie(
                            name=version.name,
                            filepath=str(media_file),
                            channel=channel_index,
                            frame_start=version.frame_start,
                        )
                    except RuntimeError as e:
                        print(e, version.filepath)

                # Case of video files with the instruction to include their sound track
                elif is_video and include_sound:
                    try:
                        # First the video strip, then the audio
                        sequence = scene.sequence_editor.sequences.new_movie(
                            name=f"{version.name}",
                            filepath=str(media_file),
                            channel=channel_index + 1,
                            frame_start=version.frame_start,
                        )

                        audio_strip = scene.sequence_editor.sequences.new_sound(
                            name=f"{version.name}.audio",
                            filepath=str(media_file),
                            channel=channel_index,
                            frame_start=version.frame_start,
                        )

                    except RuntimeError as e:
                        print(e, version.filepath)

                # Case of image file
                elif is_image:
                    try:
                        sequence = scene.sequence_editor.sequences.new_image(
                            name=version.name,
                            filepath=str(media_file),
                            channel=channel_index,
                            frame_start=version.frame_start,
                        )
                    except RuntimeError as e:
                        print(e, version.filepath)

                if sequence:
                    # Add metadata custom properties
                    for k, v in version.items():
                        sequence[k] = v

                    # Handle offset if needed
                    # -----------------------
                    if version.clip_offset_start:
                        sequence.frame_offset_start = version.clip_offset_start
                        # In some cases, the sequence is moved by the change of `frame_offset_start` (case of MetaSequence)
                        if sequence.frame_final_start != version.frame_start:
                            sequence.frame_start = (
                                version.frame_start - version.clip_offset_start
                            )

                    # Preserve ratio
                    # --------------------------
                    if version.preserve_ratio:
                        # Scene settings
                        viewport_resolution = (
                            scene.render.resolution_x,
                            scene.render.resolution_y,
                        )
                        viewport_ratio = fractions.Fraction(*viewport_resolution)

                        # Media settings
                        media_info = MediaInfo.parse(str(media_file))
                        for track in media_info.tracks:
                            if sequence.type in ["MOVIE", "IMAGE"]:
                                if track.track_type in ["Video", "Image"]:
                                    media_stream = track
                                    break

                        media_resolution = (
                            media_stream.width,
                            media_stream.height,
                        )
                        media_ratio = fractions.Fraction(*media_resolution)

                        # Scale if ratio is different
                        if viewport_ratio != media_ratio:
                            # Rename source sequence
                            sequence.name = f"{version.name}.untransformed"

                            # Create effect strip for scaling
                            effect_strip = scene.sequence_editor.sequences.new_effect(
                                type="TRANSFORM",
                                name=f"{version.name}.transform",
                                channel=channel_index + 1,
                                frame_start=version.frame_start,
                                frame_end=version.frame_start + version.duration,
                                seq1=sequence,
                            )

                            if viewport_ratio > media_ratio:
                                effect_strip.scale_start_x = float(
                                    media_ratio / viewport_ratio
                                )
                            else:
                                effect_strip.scale_start_y = float(
                                    viewport_ratio / media_ratio
                                )

                            # Create the meta sequence
                            bpy.ops.sequencer.select_all(action="DESELECT")
                            effect_strip.select = True
                            sequence.select = True
                            bpy.ops.sequencer.meta_make()

                            # Cleanup the meta sequence so it is viewed as the main sequence
                            rescaled_sequence = scene.sequence_editor.active_strip
                            rescaled_sequence.name = version.name
                            rescaled_sequence.channel = channel_index
                            rescaled_sequence["media_sequence_name"] = (
                                sequence.get("media_sequence_name") or sequence.name
                            )

                            # Substitute it as the main sequence
                            sequence = rescaled_sequence

                    # Make meta sound + video
                    # ------------------------
                    if audio_strip:
                        # Keep
                        # Update audio sequence
                        audio_strip.frame_offset_start = sequence.frame_offset_start
                        audio_strip.frame_final_start = sequence.frame_final_start
                        audio_strip.frame_final_duration = sequence.frame_final_duration

                        # Make the meta strip
                        bpy.ops.sequencer.select_all(action="DESELECT")
                        sequence.select = True
                        video_sequence = sequence
                        audio_strip.select = True
                        bpy.ops.sequencer.meta_make()

                        # Cleanup the meta strip and move it to correct channel if needed
                        sequence = scene.sequence_editor.active_strip
                        sequence["media_sequence_name"] = video_sequence.name
                        sequence.name = f"{version.name}_with-audio"
                        sequence.channel = channel_index

                    # Check duration
                    if sequence.frame_final_duration != version.duration:
                        utils_core.report_message(
                            "WARNING", "wrong duration for: " + version.filepath
                        )
                        sequence.frame_final_duration = version.duration

                    # Add review settings to sequence for fast catch at drawing
                    stax_review = scene.reviews.get(version.filepath)
                    if stax_review:
                        set_sequence_attributes(sequence, stax_review)

                    # Link to element
                    sequence["element_name"] = self.name

                    # Set if has version for fast test
                    if len(self.versions) > 1:
                        sequence["has_versions"] = True

                    # Add default blend type
                    sequence.blend_type = "CROSS"

                    # Make as metasequence with wipe effect
                    _, sequence = add_wipe_effect(sequence)

                    # Make meta twice to avoid many colors in timeline, which might disturb the user
                    sequence = make_meta([sequence])

                    return sequence

        else:
            bpy.ops.wm.report_message(
                type="WARNING", message=f"No any media file found, for {self.name}"
            )

    def delete(self):
        """Delete the element in the sequencer"""
        if self.versions:
            bpy.ops.sequencer.select_all(action="DESELECT")

            editor_sequences = bpy.context.scene.sequence_editor.sequences

            # Select every built sequence
            for version in self.versions:
                version_sequence = bpy.context.scene.sequence_editor.sequences_all.get(
                    version.name
                )
                main_meta = get_parent_metas(version_sequence)[0]
                if main_meta:
                    main_meta.select = True

                    # Wipe drivers
                    wipe_sequence = get_wipe_sequence(version_sequence)
                    for driver in ["effect_fader", "blend_alpha", "blur_width"]:
                        wipe_sequence.driver_remove(driver)

            bpy.ops.sequencer.delete()


class Track(bpy.types.PropertyGroup):
    """General class for track"""

    name: bpy.props.StringProperty(name="Track Name")
    elements: bpy.props.CollectionProperty(name="Sequences", type=Element)
    displayed: bpy.props.BoolProperty(
        name="Visibility in displayed tracks", default=True
    )
    temp: bpy.props.BoolProperty(
        name="Temporay track",
        description="Track created in a specific context. Meant to be deleted in main timeline.",
    )

    def update(self):
        """Update all sequences channels based on track name->index"""
        # Update track if displayed
        if self.displayed:
            # Display elements
            for element in self.elements:
                displayed_track_index = utils_core.get_displayed_tracks().index(
                    self.name
                )
                # +1 because Blender's channels start to 1 and not 0 as the collections
                element.display(channel_index=displayed_track_index + 1)
        else:
            # Deleted elements
            # Do not use self.delete() to avoid the track to be fully deleted and not recoverable
            for element in self.elements:
                element.delete()

    def select_all_elements(self):
        """Select all elements (assets, shots...) of this track"""
        sequence_editor = bpy.context.scene.sequence_editor
        # Select sequences
        if self.elements and self.elements[0].versions:
            last_version_name = self.elements[0].versions[-1].name
            seq = get_sequence_in_list(last_version_name, sequence_editor.sequences)
            # Select whole channel
            if seq:
                seq.select = True
                sequence_editor.active_strip = seq

                bpy.ops.sequencer.select_side(side="BOTH")

    def get_channel(self):
        return bpy.context.scene.tracks.find(self.name) + 1

    def delete(self):
        """Delete the track, from the collection, delete every associated strips and move above sequences down"""
        scene = bpy.context.scene

        # Remove sequences (made this way instead of loop(self.elements)[element.delete()] for optimization)
        self.select_all_elements()
        bpy.ops.sequencer.delete()

        # Remove track
        index = scene.tracks.find(self.name)
        scene.tracks.remove(index)

        # Move above tracks down
        for i in range(index, len(scene.tracks) - 1):
            above_track = scene.tracks[i]
            above_track.select_all_elements()
        bpy.ops.transform.seq_slide(value=(0, -1))

        # Deselect sequences
        bpy.ops.sequencer.select_all(action="DESELECT")


class UserPreferences(bpy.types.PropertyGroup):
    """Every preferences Stax needs to save"""

    user_login: bpy.props.StringProperty(
        name="User login",
        default=getpass.getuser(),
        description="User Login",
    )

    user_password: bpy.props.StringProperty(
        name="User password",
        default="",
        subtype="PASSWORD",
        description="User Password",
    )

    user_save_credentials: bpy.props.BoolProperty(
        name="User Save Credentials",
        description="User credentials saved in config file",
    )

    user_auto_login: bpy.props.BoolProperty(
        name="User Auto Log In",
        description="User Auto Log In saved in config file",
    )

    logging_auto_update: bpy.props.BoolProperty(  # TODO Is it still necessary?
        name="Logging auto update",
        description="Update automatically the previously loaded timeline at logging",
    )

    cache_directory: bpy.props.StringProperty(
        name="Cache directory",
        default=utils_cache.get_default_cache_directory(),
        subtype="DIR_PATH",
        description="Path to Stax cache directory",
    )

    cache_media: bpy.props.BoolProperty(
        name="Copy media in cache",
        description="Copy all source media from timeline in Stax' cache",
    )

    cache_expiration_delay: bpy.props.IntProperty(
        name="Cache expiration delay",
        description="Delay in days before an untouched timeline cache is automatically removed.",
        default=30,
    )

    advanced_ui: bpy.props.BoolProperty(
        name="Advanced UI", description="Show advanced Stax usage UI buttons"
    )

    show_blender_ui: bpy.props.BoolProperty(
        name="Show Blender UI",
        description="Show the original Blender UI",
    )

    callback: bpy.props.EnumProperty(
        name="Callback script",
        items=utils_linker.get_linkers_list_enum(),
    )

    autosave_delay: bpy.props.FloatProperty(
        name="Autosave Delay",
        description="Elapsing time in minutes between two autosaves",
        default=5.0,
        min=1,
        unit="TIME",
    )


class StaxInfo(bpy.types.PropertyGroup):
    """Informations related to Stax"""

    version: bpy.props.StringProperty(
        name="Stax version",
        description="Stax version that generated this '.blend' file.",
    )

    session_last_update: bpy.props.StringProperty(
        name="Last update",
        description="Date of the '.blend' session file last update.",
    )


# ===============
# Content classes
# ===============
class TextComment(bpy.types.PropertyGroup):
    """OpenReviewIO TextComment object wrapper for Stax"""

    body: bpy.props.StringProperty("Body")


class TextAnnotation(bpy.types.PropertyGroup):
    """OpenReviewIO TextAnnotation object wrapper for Stax"""

    body: bpy.props.StringProperty("Body")
    frame: bpy.props.IntProperty("Starting frame")
    duration: bpy.props.IntProperty("Duration frames")


class Image(bpy.types.PropertyGroup):
    """OpenReviewIO Image object wrapper for Stax"""

    path_to_image: bpy.props.StringProperty("Path to image", subtype="FILE_PATH")


class ImageAnnotation(bpy.types.PropertyGroup):
    """OpenReviewIO ImageAnnotation object wrapper for Stax"""

    path_to_image: bpy.props.StringProperty("Path to image", subtype="FILE_PATH")
    reference_frame: bpy.props.StringProperty("Reference image", subtype="FILE_PATH")
    frame: bpy.props.IntProperty("Starting frame")
    duration: bpy.props.IntProperty("Duration frames")


class NoteReference(bpy.types.PropertyGroup):
    """Only to keep a collection of notes references"""

    name: bpy.props.StringProperty(
        "Note name", description="Identifier for Blender. Same as date"
    )


class Note(bpy.types.PropertyGroup):
    """OpenReview Note object wrapper for Stax"""

    name: bpy.props.StringProperty(
        "Note name", description="Identifier for Blender. Same as date"
    )
    date: bpy.props.StringProperty("Note creation date.")
    author: bpy.props.StringProperty("Author")
    parent: bpy.props.StringProperty("Parent note")
    replies: bpy.props.CollectionProperty(type=NoteReference)
    text_comments: bpy.props.CollectionProperty(type=TextComment)
    text_annotations: bpy.props.CollectionProperty(type=TextAnnotation)
    images: bpy.props.CollectionProperty(type=Image)
    image_annotations: bpy.props.CollectionProperty(type=ImageAnnotation)


@persistent
def update_review(self, context):
    """Update the review in OpenReviewIO cache"""
    if self.path:  # Only if a review is associated
        review = self.as_orio_review()
        review.media = Path(self.name)
        review.write(utils_cache.get_orio_default_cache_directory())


class Review(bpy.types.PropertyGroup):
    """OpenReviewIO MediaReview object wrapper for Stax"""

    # === OpenReviewIO attributes ===

    name: bpy.props.StringProperty("Path to media", update=update_review)
    notes: bpy.props.CollectionProperty("Notes of the review", type=Note)
    status: bpy.props.EnumProperty(
        items=available_statuses_callback,
        name="Review status",
        update=update_review_status,
    )
    path: bpy.props.StringProperty(
        "Path to OpenReviewIO cache review", subtype="DIR_PATH"
    )
    updated: bpy.props.BoolProperty()

    # === Utils ===
    sequence_name: bpy.props.StringProperty(
        "Name of the linked sequence in the sequencer"
    )

    def get_orio_review(self):
        """Get OpenReviewIO object related to the current Stax element

        Creates an empty one if it doesn't target a file path.
        """

        if self.path:
            return orio.load_media_review(self.path)
        else:
            return orio.MediaReview(self.name)

    def as_orio_review(self):
        """Get current Stax element as an OpenReviewIO object."""
        user_preference = bpy.context.scene.user_preferences
        orio_review = self.get_orio_review()
        orio_review.status = orio.Status(
            author=user_preference.user_login,
            state=self.status,
            date=datetime.now().isoformat(),
        )
        return orio_review

    def add_note(self, orio_note: orio.Note, write_in_cache=True) -> Union[Note]:
        """Helping function to add the given note to the Stax review and OpenReviewIO cache review

        :param note: Note to add to both reviews
        :param write_in_cache: The note is written into Stax's cache
        :return: Created Stax note
        """
        # Write modified review to Stax ORIO cache
        # TODO this is deprecated
        if write_in_cache:
            cache_review = self.as_orio_review()
            cache_review.add_note(orio_note)
            cache_review.write(utils_cache.get_orio_default_cache_directory())

        # ! Run the below code after writing because some informations have been modified by write()
        #   i.e: path of images

        # Add Stax note to Stax review
        stax_note = utils_core.get_collection_entity(self.notes, str(orio_note.date))

        # Update Stax note with ORIO note
        # Getting the attributes (name, value) of the orio_note to 'load' them in the stax_note
        # This makes the both notes parameters match
        attributes = []
        for name, value in [
            a for a in inspect.getmembers(orio_note) if not a[0].startswith("__")
        ]:
            if name == "parent" and value:  # Sentinel for 'parent' attribute
                value = str(value.date)
            attributes.append((name, value))

        # 'silent=True' to avoid console spaming because of unecessary attributes
        utils_core.load_attributes_to_property_group(attributes, stax_note, silent=True)

        # Set replies to parent note
        if orio_note.parent:
            # Get the Main parent note.
            # Stax doesn't support replies to a reply for now then it's considered as a reply to the main note
            parent_note = self.notes.get(str(orio_note.parent.date))
            while parent_note and parent_note.parent:
                new_parent_note = self.notes.get(parent_note.parent)
                if new_parent_note:
                    parent_note = new_parent_note
                else:
                    break

            reply = parent_note.replies.add()
            reply.name = stax_note.name

        # Add contents
        for i, content in enumerate(orio_note.contents):
            # Get type of content from class type

            #   Convert from PascalCase to snake_case
            collection_name = sub(
                "(?<!^)(?=[A-Z])", "_", f"{content.__class__.__name__}s"
            ).lower()

            # Get attribute
            content_collection = getattr(stax_note, collection_name)
            collection = utils_core.get_collection_entity(content_collection, str(i))

            # Load attributes
            attributes = [
                a for a in inspect.getmembers(content) if not a[0].startswith("__")
            ]

            utils_core.load_attributes_to_property_group(
                attributes, collection, silent=True
            )

        return stax_note


classes = [
    Version,
    Element,
    Track,
    UserPreferences,
    StaxInfo,
    TextComment,
    TextAnnotation,
    Image,
    ImageAnnotation,
    NoteReference,
    Note,
    Review,
]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)

    bpy.types.Scene.current_displayed_track = bpy.props.EnumProperty(
        items=current_displayed_track_items_callback,
        name="Choose displayed track",
        update=update_viewer_displayed_track,
    )

    bpy.types.Scene.reviews = bpy.props.CollectionProperty("All reviews", type=Review)
    bpy.types.Scene.stax_info = bpy.props.PointerProperty(type=StaxInfo)
    bpy.types.Scene.tracks = bpy.props.CollectionProperty(type=Track)
    bpy.types.Scene.user_preferences = bpy.props.PointerProperty(type=UserPreferences)
    bpy.types.Scene.review_session_active = bpy.props.BoolProperty()

    # Set handler
    bpy.app.handlers.frame_change_post.append(frame_change_update)


def unregister():
    del bpy.types.Scene.review_session_active
    del bpy.types.Scene.user_preferences
    del bpy.types.Scene.tracks
    del bpy.types.Scene.stax_info
    del bpy.types.Scene.reviews
    del bpy.types.Scene.current_displayed_track

    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
