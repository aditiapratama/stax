### Context

- **System description:** *(Linux, Windows, OSX)*

- **Blender version:** 
- **Stax version:** *(can be found under the `Stax` menu, middle top of the UI)*

### Description

#### What happened
[Please fill out a short description of the error here]


#### Expected behaviour
[Please describe the behaviour you expected Stax to have]

#### How to reproduce the error as simple as possible (vanilla Stax and from startup)
[Please describe the exact steps needed to reproduce the issue]
1. 


/label ~Bug