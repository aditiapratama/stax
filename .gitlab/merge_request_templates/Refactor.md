## Description of refactor



## How to test it



## Checklist

Please check if your MR fulfills the following requirements:
- [ ] Unit tests have been adapted
- [ ] Build was run locally and any changes were pushed
- [ ] Lint has passed locally without error
- [ ] CHANGELOG.md has been updated


**FYI**: @awesome_dev_too

/label ~Refactor
