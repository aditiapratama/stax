==========================
Extend Stax
==========================

.. warning:: 

    Extending Stax is still under development and the design is not properly defined. 
    By using this technics you are aware you enter a dark area and the Stax team doesn't ensures any compatibility with upcoming versions.

The python script to **extend** Stax is called *Linker*.

The *Linker* has `Callbacks`_ and can **completely** `Override`_ Stax's UI and operators.

.. important::

   Stax is based on Blender and meant to stick as much as possible to its concepts. 
   Then, knowing `Blender Python API`_ is helpful, and learning Stax's API is learning few of Blender's.

.. _`Blender Python API`: https://docs.blender.org/api/current/

.. warning::

   Because Stax was born in a studio, the script to extend Stax is called "Callbacks Script" or "Linker" as it was thought as a *callbacks* system to *link* Stax with the production manager.
   We are aware this can be confusing and we are working on unifying this concept. This will land with `Stax 3.0`_.

.. _`Stax 3.0`: https://gitlab.com/superprod/stax/-/milestones/6

.. warning::

    Current *Callbacks* system is meant to be removed: https://gitlab.com/superprod/stax/-/issues/256
    
    We strongly recommend you to only use the `Override`_ system to avoid rewritting.

.. _`Override`: `Override Classes`_

.. include:: extend/connect_linker.rst

.. include:: extend/override_classes.rst

.. include:: extend/callbacks.rst