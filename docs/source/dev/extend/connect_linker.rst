Connect a linker
================

.. highlight:: python

Scripts Directory
-----------------
By default Stax will look in it's own "callbacks/linker scripts" directory:

* Unix: ``~/stax/callbacks``
* Win: ``$APPDATA\\Stax\\callbacks``

You can copy the scripts in this folder to choose one right after Stax' startup (before logging-in!) in `Stax configuration` workspace.

.. tip::

    | A demo script is available here: https://gitlab.com/Tilix4/stax_kitsu-caminandes_demo.

Environment variables
---------------------
In priority order Stax will look to:

#. If you want only one script, add :code:`STAX_LINKER_PATH` as an environment variable pointing to your *script.py* file.
#. If you want to choose between several scripts, add :code:`STAX_LINKER_FOLDER` as an *EV* pointing to your script(s) folder.
#. If none of these *EV* are set, Stax will look in it's own `Scripts Directory`_.


