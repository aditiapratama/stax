Initialization
===============
``__init__.py``

.. automodule:: stax
  :members:
  :undoc-members:
  :show-inheritance:

Submodules
==========
.. toctree::
   :maxdepth: 4

   properties
   operators
   utils
   ui
   draw



