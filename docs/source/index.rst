================================
Welcome in Stax's documentation!
================================

.. toctree::
   :caption: Getting Started

   getting_started


.. toctree::
   :caption: Usage

   ./user/artists_reviewers/index
   ./user/technical_director/index

.. toctree::
   :maxdepth: 4
   :caption: Features

   ./features/load_timeline
   ./features/link_reviews
   ./features/publish_reviews
   ./features/update

.. toctree::
   :maxdepth: 4
   :caption: Development

   ./dev/extend
   ./dev/core_dev