======
Update
======

Simple update
-------------

To simply update your timeline with the modifications which have been made externally, just click on the **Update** button.

.. figure:: /_static/images/SessionMenu_Update.png
   :alt: The marvellous update button

   The marvellous update button

Stax will load any new note which have been done to the reviews since the previous update.

If you've loaded a timeline and the file has changed since the previous update, the timeline will be rebuilt.

.. _full-update:

Full update
-----------

If you need to reset your cache and update your timeline, do a **SHIFT+click** on the **Update** button.

.. warning::

   **This will delete your cache path**, meaning that all your sessions and cached files will be deleted.
   Make sure you don't have any unpublished review session.