.. _`publish reviews`:

==========================
Publish Reviews
==========================

Stax uses `OpenReviewIO`_ to :ref:`load <link reviews>` and publish review files.

.. _OpenReviewIO: https://pypi.org/project/openreviewio/

Publish the edited reviews into the linked reviews directory.
When a reviews directory has been linked, clicking on :ref:`Publish Reviews <publish>` will automatically updates or create the reviews in the linked directory by adding all the notes created during the session (see :ref:`edit`).

If no any reviews directory has been linked, clicking on ``Publish Reviews`` will prompt a file browser, asking to choose a directory.

Python
------
:attr:`~stax.ops.ops_session.STAX_OT_publish_reviews`


.. highlight:: python
.. code-block:: python

    bpy.ops.scene.publish_reviews()
