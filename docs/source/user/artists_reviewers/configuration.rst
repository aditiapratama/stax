==============
Configure Stax
==============


You can access Stax’s configuration using the **User Configuration** tab.

User configuration
===================

.. _set-external-image-editor:

Set external image editor
=========================

You have to target the image editor executable via a file browser.

#.  Click on the **folder** icon
#.  Go search for your editor executable file
#.  Click **Accept**
#.  Click **Save User Configuration**
#.  That’s it

