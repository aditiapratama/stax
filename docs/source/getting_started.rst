===========
Description
===========

Stax is the best open source tool designed for reviewing.

============
Installation
============
#. Go to gitlab repository `releases page`_ and download the ``Application Template`` link, it's a ``.zip`` version of Stax.
#. Check the related Blender version.
#. Go to `Blender download page`_, download the required version and install it.
#. Start Blender.
#. With the Blender icon menu button, top left corner, click on `Install Application Template` and target downloaded ``stax.zip`` file.
#. Start Stax using ``File > New > Stax``.

.. warning:: As Stax is installing python dependencies into Blender's python, you may have to restart Stax if it's launched for the first time.

.. _`releases page`: https://gitlab.com/superprod/stax/-/releases
.. _`Blender download page`: https://www.blender.org/download/

===============
First steps
===============

Add some media
==============
First thing first, you must add some media to the timeline to be able to review it.

Drag and drop
-------------
To do it quickly, you can drag and drop a video media from your file explorer into the **timeline area**. 
You must see the media sequence appear and you must be able to play it.

Load a timeline
---------------
You can :ref:`load <load timeline>` a whole timeline using an editing file. 

.. warning:: Currently only ``.otio`` files with a specific structure can be imported.  
   :ref:`More information <load timeline>`.

#. Click on the ``Load Timeline`` button.
#. Navigate to the timeline ``.otio`` file you want to load.
#. Click on the ``Load Timeline`` button on the right bottom corner.

.. tip::

   You can have a look at the **terminal window** to see what’s happening.

When it’s done, you should see your media loaded in the timeline.

.. figure:: /_static/images/StaxLoaded.png
   :alt: Stax is ready

   Stax is ready

You can now see how to :ref:`use the review tools <reviewing-main-workspace>`.

==============
Session
==============

A session is a group of sequences linked with their related reviews. The ``Session`` menu is in the top left corner.

Manage your reviews
===================
If you want to synchronize your reviews with somebody else or accross softwares supporting OpenReviewIO standard, you can target the same reviews directory with the  ``Link Reviews`` button in the ``Session > Reviews`` menu.

.. figure:: /_static/images/ReviewsMenu_Link.png
        :alt: Link Reviews Button
        
        Link Reviews Button

Once this directory is set, :ref:`published <publish reviews>` reviews will be written into it, without asking you anything. This way you can easily share them or organize your data as you like.

If you don't do that at the start of your review session, a directory to publish the reviews you've made will be asked to you when :ref:`publishing <publish reviews>`.

Update your session
===================
If you're working with different softwares or in a team (most likely both), you will want keep the timeline and the reviews up-to-date.
The update button is in the ``Session`` menu of Stax and shown only if you've loaded a timeline or linked reviews.

.. include:: /features/update.rst
   :start-after: button.
   :end-before: Full update